* TODO [0/3][0%]

- [ ] Check variables for errors before running
- [ ] Allow custom character sets from file
- [ ] Check that all characters in original number are in the data set for the given base (make sure 9 is not used in a base 5 number)
